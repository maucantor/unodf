# The {UNODF} R package repository 

Nestedness metric for one-mode networks
------------------------------------------------
#### Authors
Mauricio Cantor, Mathias M. Pires, Diego R. Barneche

#### Maintainer and contact: 
Mauricio Cantor

Department of Biology, Dalhousie University, 1355 Oxford Street, Halifax, NS B3H 4J1, Canada. 

Current address (2016): Departamento de Ecologia e Zoologia, Universidade Federal de Santa Catarina, Caixa Postal 5102, CEP 88040-970, Florianopolis, SC, Brazil. 

Email: m.cantor@ymail.com 

Telephone: +1 (902) 703-0915

#### Reference
[Cantor M, Pires MM, Marquitti FDM, Raimundo RLG, Sebastian-Gonzalez E, Coltri P, Perez I, Barneche D, Brandt DYC, Nunes K, Daura-Jorge FG, Floeter SR, Guimaraes PR Jr. 2017. Nestedness across biological scales. PLoS ONE 12(2): e0171691. doi:10.1371/journal.pone.0171691](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0171691)



#### VERSION 1.2


This package provides a nestedness metric, UNODF, that generalizes the original metric Nestedness based on Overlap and Decreasing Fill (NODF) by Almeida-Neto et al. (2008, Oikos 117:1227-1239) for Unipartite graphs. UNODF allows the assessment of nestedness to any kind of network, including one-mode, undirected and directed networks. It also contains a null model to test the significance of empirical UNODF-values.


#### Usage and citation
The R package {UNODF} is a supplementary material of the following manuscript. If you use of it, please cite the article and refer to this repository:

```
#!r
Cantor M, Pires MM, Marquitti FDM, Raimundo RLG, Sebastian-Gonzalez E, Coltri P, Perez I, Barneche D, Brandt DYC, Nunes K, 
Daura-Jorge FG, Floeter SR, Meyer D, Guimaraes PR Jr. 2017. Nestedness across biological scales. PLoS ONE 12(2): e0171691.
doi:10.1371/journal.pone.0171691
```

and its related R package:
```
#!r
citation("UNODF")
> Cantor M, Pires MM, Barneche DR (2016). UNODF: Nestedness metric for one-mode networks. R package version 1.2.
```





#### Installation
The package can be installed from the repository as follows: 

1. Launch R. 
2. Install and/or load the package ‘*devtools*’: 
```
#!r
if(!require(devtools)){install.packages('devtools'); library(devtools)} 
```
3. Install and load the package ‘*UNODF*’: 
```
#!r
install_bitbucket(“maucantor/unodf”); library(UNODF)
```